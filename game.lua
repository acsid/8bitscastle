
bump 	= require "libs.bump"
anim8 	= require "libs.anim8"
mapManager = require "mapmanager"
--plyr = require "player"
entity = require "entity"
class = require "libs.middleclass"
local color = require "libs.colorise"

inspect = require "libs.inspect"

sounds	=	require	"sounds"

game = {
	score = 0,
	level = 1,
	init = 0
}


--console 
local consoleBuffer = {}
local consoleBufferSize = 15
for i=1,consoleBufferSize do consoleBuffer[i] = "" end
--print message to ingame console consolePrint("string")
function consolePrint(msg)
  table.remove(consoleBuffer,1)
  consoleBuffer[consoleBufferSize] = msg
  print(msg)
end
--draw ingame console
local function drawConsole()
  local str = table.concat(consoleBuffer, "\n")
  for i=1,consoleBufferSize do
    love.graphics.setColor(255,255,255, i*255/consoleBufferSize)
    love.graphics.printf(consoleBuffer[i], 10, 580-(consoleBufferSize - i)*12, 790, "left")
  end
end 

local maploaded = 0

local layer = {}
--debug
local maxgarbage = 0
local avggarbage = 0
local hip = 10
--draw debug message on screen
function drawDebug(x,y) 
  local statistics = ("fps: %d, mem: %dKB avg: %dKB Max: %dKB , Col: %d Entity: %s"):format(love.timer.getFPS(), collectgarbage("count"),avggarbage,maxgarbage, world:countItems(), table.getn(layer.entity))
  	love.graphics.setColor(255, 0, 0)
	love.graphics.print('debug',70,0)
  love.graphics.setColor(255, 255, 255)
  love.graphics.printf(statistics, 0, 0, 600, 'right')
end

function countGarbage()
if ( maxgarbage < collectgarbage("count")) then
	maxgarbage = collectgarbage("count")
end
	if (hip < 0) then
		avggarbage = (avggarbage + collectgarbage("count")) / 2
		hip = 50
	end
	hip = hip - 1
end


--spawn object
function spawn(objects)
	

	
	local obj = objects or map.objects
	
	entity.spawner(obj)
	-- print(inspect(obj))
	-- for k , object in pairs(obj) do
	
	-- if object.name == "Player" then	
	
	-- elseif object.name == "Teleport" then
	-- else
	-- local items, leng = world:queryPoint(object.x+1,object.y+1)
	-- print(("leng %s"):format(leng))
	-- if (leng < 1) then
		-- local sprite = love.graphics.newImage(object.properties.sprite)
			-- local g = anim8.newGrid(object.properties.spriteW,object.properties.spriteH,sprite:getWidth(),sprite:getHeight())
			-- local enemy = {
				-- name = object.name,
				-- sprite = sprite,
				-- x = object.x,
				-- y = object.y,
				-- width = object.width,
				-- height = object.height,
				-- direction = 0,
				-- ani = anim8.newAnimation(g(1,'1-4'),0.3),
				-- aniR = anim8.newAnimation(g(1,'1-4'),0.3),
				-- aniL = anim8.newAnimation(g(1,'1-4'),0.3):flipH()
			-- }
			-- entity.spawn(enemy)
			-- end
			-- end
	-- end
	
	--layer.entity = entity.getObject()
	
	
end

-- loadMap()
function loadMap(mapFile,x,y,direction)
	entity.clear()
	world = bump.newWorld()
	map = sti(mapFile,{"bump"})
	map:bump_init(world)
	layer = map:addCustomLayer("Sprites",5)
	--print( inspect(layer.entity))
	 -- Get player spawn object
    
    -- for k, object in pairs(map.objects) do
        -- if object.name == "Player" then	
		-- local sprite = love.graphics.newImage("files/sprites/player.png")
		-- local g = anim8.newGrid(16,16,sprite:getWidth(),sprite:getHeight())
       -- local player = object	
		-- player.entity = {
			-- name   = "Player",
			-- sprite = sprite,
			-- x      = x or player.x,
			-- y      = y or player.y,
			-- ox     = sprite:getWidth() / 2,
			-- oy     = sprite:getHeight() / 2,
			-- width	= player.width,
			-- height 	= player.height,
			-- maxJump = 35 ,
			-- jumpForce = 10,
			-- direction = 1,
			-- jump = 0,
			-- hp = 10,
			-- maxHp = 10,
			-- level = 1,
			-- isPlayer = true,
			-- isAttacking = 0,
			-- hasDamaged = 0,
			-- dmgW = 8,
			-- dmgH = 4,
			-- aniUpR = anim8.newAnimation(g(1,1,2,1,3,1,4,1),0.3),
			-- aniDownR = anim8.newAnimation(g('1-4',2),0.3),
			-- aniUpL = anim8.newAnimation(g(1,1,3,1,3,1,4,1),0.3):flipH(),
			-- aniDownL = anim8.newAnimation(g('1-4',2),0.3):flipH(),
			-- aniDownIR = anim8.newAnimation(g(1,2),0.3),
			-- aniDownIL = anim8.newAnimation(g(1,2),0.3):flipH(),
			-- animationF = anim8.newAnimation(g('1-4',1),0.3):flipH(),
			-- aniAtkR	=	anim8.newAnimation(g('1-3',3),0.08,'pauseAtEnd'),
			-- aniAtkL	=	anim8.newAnimation(g('1-3',3),0.08,'pauseAtEnd'):flipH()
		-- }
        -- objplayer = plyr:new(player)
		-- entity.spawn(player)
        -- elseif object.name == "Teleport" then
			-- world:add(object,object.x,object.y,16,16)
		-- elseif object.name == "MapInfo" then
			-- consolePrint(('MapInfo Name: %s '):format(object.properties.name))
			-- mapManager.showSplash(object)
	
		-- end
		
    -- end
	
	--spawn objects
	
	spawn()
	
	--add all entity to the layer
	
	layer.entity = entity.getObject()
	

    -- Add controls to player
    layer.update = function(self, dt)
		--print(('update %s'):format(self.name))
		for _, ent in ipairs(self.entity) do
				entity.update(ent,dt)
				if (entity.getHp(ent) == 0) then
					entity.drop(ent)
					print("dead")
				end
	  end
	  entity.postUpdate(dt)
	  entRemove()
      -- entity.update(self,dt)
	end
   -- Draw player
    layer.draw = function(self)
		for _, ent in ipairs(self.entity) do
		 entity.draw(ent)
		end
		
		-- if ( entity.draw(self) ~= nil ) then
			
		-- end
		
    end
	
	--print(inspect(map:getLayerProperties("background")))
	local bgProp = map:getLayerProperties("background")
	mapManager.setBG1(color.hex2rgba(bgProp.color))
	local wallProp = map:getLayerProperties("Collidable")
	mapManager.setWall1(color.hex2rgba(wallProp.color))
	
	
	--consolePrint(("Map Loaded %s"):format(mapFile))
	entity.unPause()
	
end

--remove entity from map layer
function entRemove()
	for k,v in ipairs(layer.entity) do
			local chkent = layer.entity[k]
		if chkent.hp == 0 then
			print(("Destroying entity %s"):format(chkent.entityId))
			table.remove(layer.entity,k)
			world:remove(chkent)
		end
	
	end
end


game.debug = true

-- if game.debug then
	-- debug.debug()
-- end
--initialize the game
function game:init()
	camera = Camera(0,0,3)
	entity.init()
	game.canvas = love.graphics.newCanvas(g_screenres.w,g_screenres.h)
	game.tmp_canvas = love.graphics.newCanvas(g_screenres.w, g_screenres.h)
	consolePrint("Game Started")
	love.physics.setMeter(16)
	consolePrint("Load Default Map")
	loadMap("files/map/debugmap.lua")
	
	
	
end

function game:update(dt)
	local game = game
	countGarbage()
	Timer.update(dt)
	map:update(dt)

end

	local translateX = 0
	local translateY = 0

function game:draw()
	love.graphics.setColor(255, 255, 255)
	local scale = 2
	local screen_width = love.graphics.getWidth() / scale
	local screen_height = love.graphics.getHeight() / scale


	--attach camera
	
	camera:attach()
	--draw layers
	mapManager.Fx("bg1")
	map:drawTileLayer("background")
	mapManager.Fx("wall1")
	map:drawTileLayer("Collidable")
	love.graphics.setColor(255,255,255)
	map.layers["Sprites"]:draw()
	
	--debug collision
	
	--map:bump_draw(world)
	--detach camera
	camera:detach()
	mapManager.draw()
	love.graphics.print('8bitsCastle',0,0)
	drawConsole()
	drawDebug()

end


function game:keypressed(k)
  if k=="escape" then love.event.quit() end
  --if k=="tab"    then shouldDrawDebug = not shouldDrawDebug end
  if k=="delete" then 
	collectgarbage("collect")
	maxgarbage = 0
  end
  if k=="tab" then
	if ( mapManager.getFx() == "rnd" ) then
		mapManager.chgFx("fire")
	elseif ( mapManager.getFx() == "fire" ) then
		mapManager.chgFx("none")
	else 
		mapManager.chgFx("rnd")
	end
	consolePrint(("Change Map FX to %s"):format(mapManager.getFx()))
  end
  if k=="r" then
	spawn()
  end

end
