local bat = {
	hp = 1,
	maxHp = 2
}

function bat.update(self,dt)
	if self.hp == nil then
		self.hp = bat.hp
		consolePrint("set bat hp")
		
	end
				dy =  math.random(-0.01,0.01)
				dx = 0
				--beta brain
					if ( self.brain == nil ) then
						self.brain = 1
						self.brainMax = 300
						self.brainNow = 10
					end
					if ( self.brain == 1) then
					dx = 1
					self.ani = self.aniR
					end
					if (self.brain == 0) then
					dx = -1
					
					self.ani = self.aniL
					end
					if (self.brainNow <= 0) then
						
						if (self.brain == 1) then
							self.brain = 0
						else
							self.brain = 1
						end
					self.brainNow = self.brainMax
					end
					self.brainNow = self.brainNow - 1
					self.x, self.y, cols, cols_len = world:move(self,self.x + dx, self.y + dy)
					--self.ani = self.aniR
					for i=1,cols_len do
						local col = cols[i]
						--consolePrint(("bat Collision with %s."):format(col.other.name))
						self.brainNow = 0
					end
					self.ani:update(dt)
end

return bat
