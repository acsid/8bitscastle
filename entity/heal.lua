local class = require "libs/middleclass"

	local sprite = love.graphics.newImage("files/sprites/objects.png")
local g = anim8.newGrid(8,8,sprite:getWidth(),sprite:getHeight())

local  heal = class('heal')

function heal:update(self,dt)
	if (self.lifetime == nil) then
		self.lifetime = heal.lifetime
		self.hp = heal.hp
	end
	dy = mapManager.getGravity()
	local goalX, goalY = self.x, self.y + dy
	local aX, aY, acols, alen = world:check(self,goalX,goalY)
	self.x, self.y = aX, aY
	self.lifetime = self.lifetime - 1
	if (self.lifetime == 0) then
		self.hp = 0
	end	
	world:update(self,aX,aY)
	end
function heal:initialize()
	self.sprite = love.graphics.newImage("files/sprites/objects.png")
	self.pickupable = 1
	self.name	= "heal"
	self.hp = 1
	self.lifetime = 300
	self.width 	=	 8
	self.height	=	8
	self.x = 0
	self.y = 0
	self.ani = anim8.newAnimation(g(1,'1-4'),0.3)
	self.aniR = anim8.newAnimation(g(1,'1-4'),0.3)
	self.aniL = anim8.newAnimation(g(1,'1-4'),0.3):flipH()
end

function	heal:entity(object) 
			obj = heal
			setmetatable(obj,self)
			self.__index = self
			return obj
			end
return heal
