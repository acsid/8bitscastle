player	=	require("entity.player")
zombie 	=	require("entity.zombie")
bats 	=	require("entity.bats")
heal	=	require("entity.heal")


local empty = {
			name   = "Empty",
			x      = 0,
			y      = 0,
			width	= 16,
			height 	= 32,
			maxJump = 35 ,
			jumpForce = 10,
			direction = 1,
			jump = 0,
			hp = 10,
			maxHp = 10,
			level = 1, 
			isPlayer = false,
			isAttacking = 0,
}

local entity = {}

local objects = {}


local aniUp = {}
local aniDown = {}
local playerjump = 0

local entityId = 0


local pause = 0
local teleporter = {}

--spawn a new entity 
function entity.spawn(object)
	--consolePrint(("Spawn %s x: %s y: %s w: %s h: %s"):format(object.name,object.x,object.y,object.width,object.height))
	--object.brain = 0
	
	print(inspect(object))
	entityId = entityId + 1
	object.entityId = entityId
	world:add(object,object.x,object.y,object.width,object.height)
	table.insert(objects,object)
end

function entity.spawner(obj)
--print(inspect(object))


for k , object in pairs(obj) do
	
	if object.name == "Player" then	
	
	
	local objc = player:new(object)
	
	
	entity.spawn(objc:getEntity())
	
	elseif object.name == "Teleport" then
	else
	local items, leng = world:queryPoint(object.x+1,object.y+1)
	-- print(("leng %s"):format(leng))
	if (leng < 1) then
		local sprite = love.graphics.newImage(object.properties.sprite)
			local g = anim8.newGrid(object.properties.spriteW,object.properties.spriteH,sprite:getWidth(),sprite:getHeight())
			local enemy = {
				name = object.name,
				sprite = sprite,
				x = object.x,
				y = object.y,
				width = object.width,
				height = object.height,
				direction = 0,
				ani = anim8.newAnimation(g(1,'1-4'),0.3),
				aniR = anim8.newAnimation(g(1,'1-4'),0.3),
				aniL = anim8.newAnimation(g(1,'1-4'),0.3):flipH()
			}
			entity.spawn(enemy)
			end
			end
	end

end

--return object table
function entity.getObject()
	return objects
end
--clear entity table
function entity.clear()
	pause = 1
	objects = {}
	
end

function entity.unPause()
	pause = 0
end

function entity.init()
	objSprite = love.graphics.newImage("files/sprites/objects.png")
end

--teleport entity to new map
function entity.teleportTo(mapFile,toX,toY,direction)
	teleporter = {
		mapFile = mapFile,
		toX = toX,
		toY = toY,
		teleport = true
	}
end

function entity.postUpdate(dt)
	if (teleporter.teleport == true) then
		map:removeLayer("Sprites")
		loadMap(teleporter.mapFile,teleporter.toX,teleporter.toY,1)
		teleporter.teleport = false
	end
	
end
tempplayerX ,  tempplayerY = 0,0
--update entity
function entity.update(self,dt) 
	if (pause == 0) then 
		--self:update(dt)
		if (self.isPlayer == true) then 
		 player:update(self,dt)
		 tempplayerX , tempplayerY = self.x, self.y
	 end
	end
	-- if (self.isPlayer == true) then 
		-- player:update(dt)
		-- tempplayerX , tempplayerY = self.x, self.y
	-- end
		-- update enemy AI if in range of the player
		-- if ( self.x > tempplayerX - 300 ) then 
			-- if ( self.x < tempplayerX  + 300 ) then 
				-- if (self.name == "Zombie") then
					-- zombie.update(self,dt)
				-- elseif (self.name == "Bat") then
					-- bats.update(self,dt)
				-- elseif (self.name == "heal") then
					-- self:update(dt)
				-- end
			-- end
		
		-- end
	-- end
	
end
--draw entity
function entity.draw(self)
	if (self.isPlayer == true) then
		player.draw(self)
	elseif (self.name == "Teleport") then 
		-- do nothing
	else
		--print(("draw %s"):format(self.name))
		self.ani:draw(self.sprite,self.x,self.y)
	end
end

function entity.doDamage(self,hit)
	if (self.hp == nil) then
	else
		self.hp = self.hp - hit
		consolePrint(("%s (%s) hit %s point %s hp"):format(self.name,self.entityId,hit,self.hp))
	end
end

function entity.getPlayer(dt) 
  for k, object in pairs(objects) do
	if (object.isPlayer == true) then
		return object
	end
	return empty
  end
	return empty
end

function entity.getHp(self)
	return self.hp
end

function entity.drop(self)
	local ent = heal:new(self)
	entity.spawn(ent)

end


return entity