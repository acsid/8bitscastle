gamestate 	= require "libs.hump.gamestate"
sti			= require "libs.sti"
Timer		= require "libs.hump.timer"
Camera 	= require "libs.hump.camera"
require "menu"
require "game"

g_screenres = {
    w=math.floor(love.graphics.getWidth()/2),
    h=math.floor(love.graphics.getHeight()/2)
}

gameVersion = "0.0.7"


function love.load()
	love.window.setTitle(("8bits Castle - %s "):format(gameVersion))
	gamestate.registerEvents()
	gamestate.switch(menu)

end