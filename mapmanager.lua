local mapmanager = {}

local currentFx = "none"
--layers tint
local bg1RGB = { r = 255, g = 255, b = 0 }
local bg2RGB = { r = 255, g = 255, b = 255 }
local wall1RGB = { r = 255, g = 100, b = 0 }
local wall2RGB = { r = 255, g = 255, b = 255 }
local mapSplash = ''
local gravity = 1
-- map Effects
function mapmanager.Fx(layer)
	if (currentFx == "rnd") then
		r, g, b = math.random(0,255), math.random(0,255), math.random(0,255)
		love.graphics.setColor(r,g,b)
	elseif (currentFx == "fire") then
		r, g, b = 255, math.random(0,255), 0
		love.graphics.setColor(r,g,b)
	else 
		if (layer == "bg1") then
			love.graphics.setColor(bg1RGB.r,bg1RGB.g,bg1RGB.b)
		end
		if (layer == "bg2") then
			love.graphics.setColor(bg2RGB.r,bg2RGB.g,bg2RGB.b)
		end
		if (layer == "wall1") then
			love.graphics.setColor(wall1RGB.r,wall1RGB.g,wall1RGB.b)
		end
		if (layer == "wall2") then
			love.graphics.setColor(wall2RGB.r,wall2RGB.g,wall2RGB.b)
		end
	end
end

function mapmanager.setBG1(r,g,b,a)
	print(("set BG1: r %s g: %s b: %s"):format(r,g,b))
	bg1RGB = { r = r,g = g,b = b}
end
function mapmanager.setBG2(r,g,b,a)
	print(("set BG2: r %s g: %s b: %s"):format(r,g,b))
	bg2RGB = { r = r,g = g,b = b}
end
function mapmanager.setWall1(r,g,b,a)
	print(("set Wall1: r %s g: %s b: %s"):format(r,g,b))
	wall1RGB = { r = r,g = g,b = b}
end


function mapmanager.chgFx(Fx)
	currentFx = Fx
end
function mapmanager.getFx()
	return currentFx
end

function mapmanager.showSplash(object)
	mapSplash = object.properties.name
	Timer.after(4, function() mapSplash = '' end )
end

function mapmanager.drawSplash()
	if (mapSplash == '') then
		--do nothing
	else
		love.graphics.printf(mapSplash,0,100,love.graphics.getWidth(),"center")
	end
end

function mapmanager.draw()
	mapmanager.drawSplash()
end

function mapmanager.getGravity()
	return gravity
end

return mapmanager