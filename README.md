﻿8 bits Castle
--- 
Written in lua with the love2d game engine.
The project is inspired by the castlevania series for the nes for the graphics.
---
Language: lua
Maps can be made using Tiled and exported as .lua files
---
The project as progressed backward during the last few commit.
---
Todo before 0.1 release.
X = DONE, ! = In progress

- Player			 [X]
- Map Loading 		 [X]
- Map Teleporting	 [ ]
- Attack			 [X]
- Weapon Sword		 [ ]
- Zombie			 [ ]
- Bats				 [ ]
- Mobs Drop			 [ ]
- Heal Object		 [ ]
- Game Over Screen	 [ ]
- Menu Screen		 [!]
- Mobs Death		 [ ]

---
0.2+ 
- Weapon Arrow


