local class = require "libs/middleclass"

player = class('player')


local sprite = love.graphics.newImage("files/sprites/player.png")
		local g = anim8.newGrid(16,16,sprite:getWidth(),sprite:getHeight())



function player:initialize(object)
	player = object
	player.entity = {
			name   = "Player",
			sprite = sprite,
			x      = x or player.x,
			y      = y or player.y,
			ox     = sprite:getWidth() / 2,
			oy     = sprite:getHeight() / 2,
			width	= player.width,
			height 	= player.height,
			maxJump = 35 ,
			jumpForce = 10,
			direction = 1,
			jump = 0,
			hp = 10,
			maxHp = 10,
			level = 1,
			isPlayer = true,
			isAttacking = 0,
			hasDamaged = 0,
			dmgW = 8,
			dmgH = 4,
			aniUpR = anim8.newAnimation(g(1,1,2,1,3,1,4,1),0.3),
			aniDownR = anim8.newAnimation(g('1-4',2),0.3),
			aniUpL = anim8.newAnimation(g(1,1,3,1,3,1,4,1),0.3):flipH(),
			aniDownL = anim8.newAnimation(g('1-4',2),0.3):flipH(),
			aniDownIR = anim8.newAnimation(g(1,2),0.3),
			aniDownIL = anim8.newAnimation(g(1,2),0.3):flipH(),
			animationF = anim8.newAnimation(g('1-4',1),0.3):flipH(),
			aniAtkR	=	anim8.newAnimation(g('1-3',3),0.08,'pauseAtEnd'),
			aniAtkL	=	anim8.newAnimation(g('1-3',3),0.08,'pauseAtEnd'):flipH()
		}
	
end 

function player:update(dt)
	-- 96 pixels per second
			local speed = 6
			--local jumpForce = 10
			local maxVel = 150
			local gravity = mapManager.getGravity()
			local dx, dy = 0, 0
			dy = gravity + (gravity * dt)
			local cols
			self.isPlayer = true
			self.isPermanent = true
			--player = self
			-- get veolcity
		
			
				--move player left
				if love.keyboard.isDown("a") or love.keyboard.isDown("left") then
					dx = -2 + (1 * dt)
					--self.x = self.x - speed * dt
				end
				--move player right
				if love.keyboard.isDown("d") or love.keyboard.isDown("right") then
					--self.x = self.x + speed * dt
					dx = 2 + (1 * dt)
				end
				--move player up
				if love.keyboard.isDown("w") or love.keyboard.isDown("up") then
					--self.x = self.x - speed * dt
					
				end
				--move player down
				if love.keyboard.isDown("s") or love.keyboard.isDown("down") then
					--self.x = self.x - speed * dt
					
				end
				if love.keyboard.isDown("space") then
					if (self.jumpForce > 1) then
						self.jump = 2.5
						self.jumpForce = self.jumpForce - 1
					else
						
						--self.jumpForce = self.jumpForce - 1
					end
				
				else
					--self.jumpForce = self.jumForce +1
					if (self.jumpForce > self.maxJump) then
						self.jumpForce = self.maxJump
					end
				end	
				--attack for now its K
				if love.keyboard.isDown("k") then
					if self.isAttacking == 0 then
						self.isAttacking = 1
						self.aniAtkR:gotoFrame(1)
						self.aniAtkR:resume()
						self.aniAtkL:gotoFrame(1)
						self.aniAtkL:resume()
						Timer.after(0.3, function() 
							self.isAttacking = 0 
							self.hasDamaged = 0
						end )
						print("attack")
					end
				end
				if (self.jump > 0) then
					self.jump = self.jump - 0.05
				else 
					self.jump = 0
				end
				dy = dy - (self.jump + (self.jump * dt))
				
				--check if player can move to goal
				local goalX, goalY = self.x + dx , self.y + dy	
				local aX, aY, acols, alen = world:check(self,goalX,goalY)
			local onFloor = 0
	local touchLeft = 0
	local touchRight = 0
	for i = 1 , alen do 
		local col = acols[i]
		--check if player collide with a teleporter
		if (col.other.name == "Teleport") then
					consolePrint(("Teleport to %s. x: %s, y: %s"):format( col.other.properties.map,col.other.properties.toX,col.other.properties.toY))
					entity.teleportTo(col.other.properties.map,col.other.properties.toX,col.other.properties.toY,self.direction)
					
				elseif (col.other.name == "Enemy") then
					consolePrint("knock")
					
					self.x, self.y, kcols, kcols_len = world:move(self,self.x -10, self.y + dy)
				elseif (col.other.name == "heal") then
					consolePrint(("heal player %s hp %s"):format(col.other.hp,self.hp))
					self.hp = self.hp + col.other.hp
					col.other.hp = 0
				end
				
				--ic player touching left right floor
		if (col.other.name == nil ) then
		--print(("normal: %s %s"):format(col.normal.x,col.normal.y))
			if col.normal.y == -1 then
					onFloor = 1
					self.jumpForce = self.maxJump
				end
			if ( col.normal.x == -1 ) then
					touchRight = 1
			end
			if ( col.normal.x == 1 ) then
					touchLeft = 1
			end
			end
		
		
	end
	--make player climb if he can
	if (touchRight == 1) then
		aX, aY, acols, alen = world:check(self,aX+1,aY-6)
	end
	if (touchLeft == 1) then
		aX, aY, acols, alen = world:check(self,aX-1,aY-6)
	end
	--print(("g:%s r:%s l:%s "):format(onFloor,touchRight,touchLeft))
	--update position in world_
	world:update(self,aX,aY)
	
	--self.x, self.y, cols, cols_len = world:move(self,self.x + dx, self.y + dy)
	
	self.x = aX
	self.y = aY
			
		--update animation IDLE
			if (dx == 0) then
				if (self.direction < 1 ) then
				aniUp = self.aniUpL
				aniDown = self.aniDownIL
				else
				aniUp = self.aniUpR
				aniDown = self.aniDownIR
				end
				
			elseif  (dx<0) then
				self.direction = 0
				aniUp = self.aniUpL
				aniDown = self.aniDownL
			else
				self.direction = 1
				aniUp = self.aniUpR
				aniDown = self.aniDownR
			end
			
			--attack animation
			if (self.isAttacking == 1) then
				if (self.direction == 1) then
					aniUp = self.aniAtkR
				else
					aniUp = self.aniAtkL
				end
				local htX, htY = 0 ,0 
				if (aniUp.position == 3) then
					if (self.direction == 1) then
						htX, htY = self.x + 17, self.y+8
					else
						 htX, htY = self.x - 1, self.y+8
					end
						local items, leng = world:queryPoint(htX,htY)
						for i = 1 , leng do
						if (self.hasDamaged == 0) then
							local hit = items[i]
							entity.doDamage(hit,1)
							--play hitsound
							sounds.play(sounds.hit1)
							self.hasDamaged = 1
						end
					end
				end
			end
			--print(inspect(aniUp))
			self.ani = aniUp
			aniUp:update(dt)
			aniDown:update(dt)
			self.x,self.y = math.floor(self.x), math.floor(self.y)
			--self.x, self.y = self.x, self.y
end
--draw player
function player.draw(self)
		aniUp:draw(self.sprite,math.floor(self.x), math.floor(self.y))
		aniDown:draw(self.sprite,math.floor(self.x), math.floor(self.y+16))
		camera:lookAt(math.floor(self.x),math.floor(self.y))
		--love.graphics.print(("x: %s x: %s"):format(self.x,self.y),self.x,self.y)
end

function player.attack()
	
end

return player