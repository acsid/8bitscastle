local sounds = {
	hit1 = love.audio.newSource("files/sounds/hit1.wav","static")	
}

function sounds.play(sound)
	love.audio.play(sound)
end


return sounds